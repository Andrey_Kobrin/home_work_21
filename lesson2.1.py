def get_recipe():

	cook_book = {}

	with open('dom.txt') as f:
		for line in f:
			dish_name = line.rstrip()
			amount_of_ingridients = f.readline().rstrip()
			amount_of_ingridients = int(amount_of_ingridients)
			book = []
			for _ in range(amount_of_ingridients):
				ingridient = f.readline().rstrip()
				ingridient = ingridient.split('|')
				
				dish = {}
				dish['ingridient_name'] = ingridient[0] 
				dish['quantity'] = int(ingridient[1])
				dish['measure'] = ingridient[2]

				book.append(dish)
			
			cook_book[dish_name] = book 

		return(cook_book)
	


def get_shop_list_by_dishes(dishes, person_count, cook_book):
  shop_list = {}
  for dish in dishes:
    for ingridient in cook_book[dish]:
      new_shop_list_item = dict(ingridient)
      new_shop_list_item['quantity'] *= person_count
      if new_shop_list_item['ingridient_name'] not in shop_list:
        shop_list[new_shop_list_item['ingridient_name']] = new_shop_list_item
      else:
        shop_list[new_shop_list_item['ingridient_name']]['quantity'] += new_shop_list_item['quantity']
  return shop_list

def print_shop_list(shop_list):  
   for shop_list_item in shop_list.values():
    print('{ingridient_name} {quantity} {measure}'.format(**shop_list_item))
    
def create_shop_list():
  person_count = int(input('Введите количество человек: '))
  cook_book = get_recipe()
  dishes = input('Введите блюда в расчете на одного человека (через запятую): ').split(', ')
  shop_list = get_shop_list_by_dishes(dishes, person_count, cook_book)
  print_shop_list(shop_list)
  
create_shop_list()








		